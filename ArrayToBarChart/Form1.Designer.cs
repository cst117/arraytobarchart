﻿/**
 * Develop a GUI that reverses an array
 * 
 * Allow user to choose a random input or to input the array manually. Display the
 * array in a bar chart on the left of a form. Provide a button, and when the user
 * presses the button do one more interchange of elements, displaying the partially
 * reversed array on the right of the form. When the reversal is complete, disable 
 * the button.
 */

namespace ArrayToBarChart
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.reverseBtn = new System.Windows.Forms.Button();
            this.randomArray = new System.Windows.Forms.Button();
            this.arrayAddBox = new System.Windows.Forms.TextBox();
            this.addToArray = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.arrayDisplay = new System.Windows.Forms.Label();
            this.errMsgs = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(500, 200);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPaint);
            // 
            // reverseBtn
            // 
            this.reverseBtn.Location = new System.Drawing.Point(518, 166);
            this.reverseBtn.Name = "reverseBtn";
            this.reverseBtn.Size = new System.Drawing.Size(254, 23);
            this.reverseBtn.TabIndex = 1;
            this.reverseBtn.Text = "Step: Reverse Array";
            this.reverseBtn.UseVisualStyleBackColor = true;
            this.reverseBtn.Click += new System.EventHandler(this.reverseBtn_Click);
            // 
            // randomArray
            // 
            this.randomArray.Location = new System.Drawing.Point(518, 110);
            this.randomArray.Name = "randomArray";
            this.randomArray.Size = new System.Drawing.Size(254, 23);
            this.randomArray.TabIndex = 2;
            this.randomArray.Text = "Generate Random Array";
            this.randomArray.UseVisualStyleBackColor = true;
            this.randomArray.Click += new System.EventHandler(this.randomArray_Click);
            // 
            // arrayAddBox
            // 
            this.arrayAddBox.Location = new System.Drawing.Point(518, 81);
            this.arrayAddBox.Name = "arrayAddBox";
            this.arrayAddBox.Size = new System.Drawing.Size(67, 20);
            this.arrayAddBox.TabIndex = 3;
            // 
            // addToArray
            // 
            this.addToArray.Location = new System.Drawing.Point(591, 81);
            this.addToArray.Name = "addToArray";
            this.addToArray.Size = new System.Drawing.Size(181, 23);
            this.addToArray.TabIndex = 4;
            this.addToArray.Text = "Add To Array";
            this.addToArray.UseVisualStyleBackColor = true;
            this.addToArray.Click += new System.EventHandler(this.addToArray_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(518, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(238, 52);
            this.label1.TabIndex = 5;
            this.label1.Text = "Create an array to start playing with this program. \r\nThe array will consist of o" +
    "nly numbers and range \r\nfrom 1 to 200. You can also generate an array \r\nrandomly" +
    ".";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(518, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Results: ";
            // 
            // arrayDisplay
            // 
            this.arrayDisplay.AutoSize = true;
            this.arrayDisplay.Location = new System.Drawing.Point(518, 150);
            this.arrayDisplay.Name = "arrayDisplay";
            this.arrayDisplay.Size = new System.Drawing.Size(68, 13);
            this.arrayDisplay.TabIndex = 7;
            this.arrayDisplay.Text = "Array Display";
            // 
            // errMsgs
            // 
            this.errMsgs.ForeColor = System.Drawing.Color.DarkRed;
            this.errMsgs.Location = new System.Drawing.Point(518, 65);
            this.errMsgs.Name = "errMsgs";
            this.errMsgs.Size = new System.Drawing.Size(254, 13);
            this.errMsgs.TabIndex = 8;
            this.errMsgs.Text = "Error Messages";
            this.errMsgs.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(697, 195);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(75, 23);
            this.clearBtn.TabIndex = 9;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(784, 226);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.errMsgs);
            this.Controls.Add(this.arrayDisplay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addToArray);
            this.Controls.Add(this.arrayAddBox);
            this.Controls.Add(this.randomArray);
            this.Controls.Add(this.reverseBtn);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Array To Bar Chart Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button reverseBtn;
        private System.Windows.Forms.Button randomArray;
        private System.Windows.Forms.TextBox arrayAddBox;
        private System.Windows.Forms.Button addToArray;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label arrayDisplay;
        private System.Windows.Forms.Label errMsgs;
        private System.Windows.Forms.Button clearBtn;
    }
}

