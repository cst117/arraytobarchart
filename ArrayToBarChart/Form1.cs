﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArrayToBarChart
{
    public partial class Form1 : Form
    {
        private int?[] numArray = new int?[10];     // Nullable int
        private const int CHART_HEIGHT = 200;
        private const int CHART_WIDTH = 500;
        private const int BAR_WIDTH = 50;

        private int currentFirstIndex = 0;          // Settings for doing reversal step-by-step
        private int currentSecondIndex = 0;

        public Form1()
        {
            InitializeComponent();

            arrayDisplay.Text = "";
            errMsgs.Text = "";
            reverseBtn.Enabled = false;
        }

        /**
         *  Updates the label for the array display. Will show under "Results:".
         */
        public void updateArrayDisplay()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");

            for (int i = 0; i < 10; i++)
            {
                if (numArray[i] != null)
                {
                    if (i != 0)
                        sb.Append(", ");

                    sb.Append(numArray[i]);
                }
            }

            sb.Append("}");

            arrayDisplay.Text = sb.ToString();
        }

        /**
         *  Button to add items to the array manually
         */
        private void addToArray_Click(object sender, EventArgs e)
        {
            errMsgs.Text = "";
            int n;

            // Make sure textbox is not empty and is an integer
            if (arrayAddBox.Text != "" && Int32.TryParse(arrayAddBox.Text, out n))
            {
                int numToAdd = Int32.Parse(arrayAddBox.Text);

                if (numToAdd <= 0 || numToAdd > 200)
                    errMsgs.Text = "Must be between 1 and 200";
                else
                {
                    randomArray.Enabled = false;

                    for (int i = 0; i < 10; i++)
                    {
                        if (numArray[i] == null)
                        {
                            numArray[i] = numToAdd;
                            break;
                        }
                        else if (i == 9)
                        {
                            errMsgs.Text = "Array is Full";
                        }
                    }
                }

                arrayAddBox.Text = "";

                updateArrayDisplay();
                reverseBtn.Enabled = true;
                Refresh();
            }
            else
            {
                errMsgs.Text = "Please enter a number";
            }
        }

        /**
         *  Button to randomly generate 10 numbers for the array
         */
        private void randomArray_Click(object sender, EventArgs e)
        {
            errMsgs.Text = "";
            Random rndmNum = new Random();

            for (int i = 0; i < 10; i++)
            {
                numArray[i] = rndmNum.Next(1, 201);
            }

            updateArrayDisplay();
            reverseBtn.Enabled = true;
            Refresh();
        }

        /**
         *  Method that will draw the chart
         */
        private void panelPaint(object sender, PaintEventArgs e)
        {
            Graphics bars = e.Graphics;
            Brush theColor;

            for (int i = 0; i < 10; i++)
            {
                if (i % 2 == 0)
                    theColor = Brushes.Gray;
                else if (i % 3 == 0)
                    theColor = Brushes.DarkGray;
                else
                    theColor = Brushes.LightGray;

                if (numArray[i] != null)
                {
                    bars.FillRectangle(theColor, i * BAR_WIDTH, CHART_HEIGHT - (Int32)numArray[i], BAR_WIDTH, (Int32)numArray[i]);
                }
            }
        }

        private void reverseBtn_Click(object sender, EventArgs e)
        {
            // Disable these buttons while we are stepping through and sorting aray
            addToArray.Enabled = false;
            randomArray.Enabled = false;

            // Make sure this next index isn't null, otherwise we are done
            if (numArray[currentFirstIndex] != null)
            {
                // Iterate through the remaining indices to compare and swap if needed
                for (int i = currentSecondIndex + 1; i < 10; i++)
                {
                    // If the first is larger than the second, then swap them
                    if (numArray[currentFirstIndex] > numArray[i])
                    {
                        int? theCurrent = numArray[currentFirstIndex];
                        numArray[currentFirstIndex] = numArray[i];
                        numArray[i] = theCurrent;
                    }
                }

                // once all swapping is done, up the currentFirstIndex if possible
                if (currentFirstIndex != 9)
                {
                    currentFirstIndex++;
                    currentSecondIndex++;
                }
                else
                    reverseBtn.Enabled = false;
            }
            else
            {
                reverseBtn.Enabled = false;
            }

            updateArrayDisplay();
            Refresh();
        }

        /*
         *  Clears the data on the screen 
         */
        private void clearBtn_Click(object sender, EventArgs e)
        {
            randomArray.Enabled = true;
            Array.Clear(numArray, 0, numArray.Length);
            arrayDisplay.Text = "";
            errMsgs.Text = "";
            arrayAddBox.Text = "";
            reverseBtn.Enabled = false;
            addToArray.Enabled = true;
            randomArray.Enabled = true;
            currentFirstIndex = 0;
            currentSecondIndex = 0;
            Refresh();
        }
    }
}
